# Description Logic Primer

This latex slide is based on the works of Markus Krotzsch, Frantisek Simancik and Ian Horrocks of the Department of Computer Science, University of Oxford - "A Description Logic Primer"

## Compiling

The default way of compiling the latex files would be to run the makefile in the root directory of this folder.

```bash
cd /path/to/folder
make
```

These files were tested with `xelatex` but should work with any other latex compiler. If you're unfimiliar with latex,

1. if you're on Windows or Mac, go to [Latex Project Page](https://www.latex-project.org/get/) and follow their instructions.
2. if you're on Debian based systems, type `apt install texlive-base` and install the various other components. If you're lazy like me type `apt install texlive-full` instead.
3. if you're on other systems, I simply have not experienced using it enough to use it with my work and I'm sorry.

## CI/CD

I have no idea how this works. But according to this user's [docker image](https://github.com/aufenthaltsraum/stuff/wiki/Using-GitLab-CI-for-Building-LaTeX), I should be able to compile a pdf file at [https://gitlab.com/imfulee/DSL/pipelines](https://gitlab.com/imfulee/DSL/pipelines)
