all:
	xelatex primer.tex
	xelatex primer.tex
	latexindent -l=localSettings.yaml primer.tex -o=primer.tex
	$(MAKE) clean

clean:
	rm *.aux *.log *.out *.nav *.snm *.toc *.fdb_latexmk *.fls